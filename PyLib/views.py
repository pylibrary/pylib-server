from django.views import View
from django.shortcuts import render_to_response
from django.conf import settings

class HomeView(View):

    template_name = 'base.html'
    data = {'title': 'Home','brand': 'PyLibrarian','runserver': settings.RUNSERVER,'tpl_path': settings.STATIC_ROOT}

    def get(self,request,*args,**kwargs):
        return render_to_response(self.template_name,self.data)
